#include <Arduino.h>
#include <Wire.h>
#include "Adafruit_TCS34725.h"
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH1106.h>


#define OFFSET  0

Adafruit_TCS34725 tcs = Adafruit_TCS34725(TCS34725_INTEGRATIONTIME_50MS, TCS34725_GAIN_4X);

// If using software SPI (the default case):
#define OLED_MOSI   9
#define OLED_CLK   10
#define OLED_DC    11
#define OLED_CS    12
#define OLED_RESET 13

Adafruit_SH1106 display(OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);
uint16_t r, g, b, c, colorTemp, lux;

void setup() {
  Serial.begin(115200);
  if (tcs.begin()) {
    //Serial.println("Found sensor");
  } else {
    Serial.println("No TCS34725 found ... check your connections");
    while (1); // halt!
  }

  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SH1106_SWITCHCAPVCC);
  // Clear the buffer.
  display.clearDisplay();
}

void print_text(){
  display.println("");
  if (r >= (255 - OFFSET) && g <= OFFSET && b <= OFFSET){
    display.println("   RED     ");
  }
  else if (r <= OFFSET && g >= (255 - OFFSET) && b <= OFFSET){
    display.println("   GREEN   ");
  }
  else if (r <= OFFSET && g <= OFFSET && b >= (255 - OFFSET)){
    display.println("   BLUE   ");
  }
  else {
    display.println(" UNKNOWN ");
  }

  enum {BufSize=32};
  char buf[BufSize];
  snprintf(buf, BufSize, "%d, %d, %d", r, g, b);
  display.println(buf);

}

void loop() {

  tcs.getRawData(&r, &g, &b, &c);
  colorTemp = tcs.calculateColorTemperature_dn40(r, g, b, c);
  lux = tcs.calculateLux(r, g, b);
  r /= 256;
  g /= 256;
  b /= 256;
  Serial.print("R: "); Serial.print(r, DEC); Serial.print(" ");
  Serial.print("G: "); Serial.print(g, DEC); Serial.print(" "); 
  Serial.print("B: "); Serial.print(b, DEC); Serial.println("   ");
  Serial.print("Lux: "); Serial.print(lux, DEC); Serial.print(" - ");

  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0, 0);
  print_text();
  display.display();

  delay(1000);
}